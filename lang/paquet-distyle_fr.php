<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'distyle_description' => 'Un thème pour le squelettes-dist de SPIP.
	
	Le fond de page est css/images/background.jpg
	qu\'on peut remplacer par squelettes/css/images/background.jpg',
	'distyle_slogan' => 'DIStyle',

);